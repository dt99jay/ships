#!/usr/bin/python

from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor, Adafruit_StepperMotor
import time, atexit, requests, xmltodict, json, math
from collections import OrderedDict

def fetchShips():

    url = "http://www.southamptonvts.co.uk/admin/ftp/Ships_Along_Side/sotberthed.xml" # Port Authority XML

    sotberthed = requests.get(url = url) # Fetch the XML
    sotberthed_xml = xmltodict.parse(sotberthed.text) # Parse into a dict
    sotberthed_json = json.loads(json.dumps(sotberthed_xml)) # Load as JSON

    categories = OrderedDict() # Establish common categories of ship

    categories = {
        "Barge (OBA)":0,
        "Chemical Tank (TCH)":0,
        "Container Carrier (UCC)":0,
        "General Cargo (GGC)":0,
        "Hopper Dredger (DHD)":0,
        "Lpg (LPG)":0,
        "Pontoon (OPO)":0,
        "Ro/Ro (URR)":0,
        "Tank (TTA)":0,
        "Waste Ship (OWA)":0,
        "Passenger (MPR)":0,
        "Patrol Ship (OPA)":0,
        "Pilot Ship (OPI)":0,
        "Research (RRE)":0,
        "Tug (XTG)":0,
        "Yacht (OYT)":0,
        "Vehicle (MVE)":0,
        "Passenger Ro/Ro (PRR)":0,
        "Barge Carrier (UBG)":0,
        "Research/Supply Ship (RRS)":0,
        "Training (OTR)":0,
        "Yacht (YACH)":0,
        "Bulk (BBU)":0,
        "Firefighting Tug (XFF)":0,
        "Dredger (DDR)":0,
        "Tug/Tender (XTT)":0,
        "Lng/Lpg (LNP)":0
    }

    for row in sotberthed_json["exportResults"]["table"]["rows"]["row"]: # For each row in JSON
        if row["type"] in categories: # If the ship type is known
            key = row["type"]
            categories[key] += 1 # Increment the tally
        else:
            print row["type"] # If an unknown ship type, print

    cargo = categories["Ro/Ro (URR)"] + categories["Container Carrier (UCC)"] + categories["Vehicle (MVE)"] + categories["General Cargo (GGC)"]
    passenger = categories["Passenger (MPR)"] + categories["Passenger Ro/Ro (PRR)"] + categories["Yacht (OYT)"]

    return cargo,passenger

def calculateSteps(currentShips):
    spoolMin = 25/2 # Minimum radius of the spool
    spoolMax = 35/2 # Maximum radius of the spool
    shipsMin = 0 # Minimum number of ships displayed
    shipsMax = 20 # Maximum number of ships displayed
    radius = spoolMax-((currentShips-shipsMin)*(spoolMax-spoolMin)/(shipsMax-shipsMin)) # Linearly scale the current number of ships into a spool radius
    circumference = 2*math.pi*radius # Calculate the circumference
    height = 65 # The distance between each ship
    revolutions = height/circumference # Revolutions required to extend / retract the right amount of material
    steps = int(revolutions*513) # The steps required by the above revolution
    return steps

def turnOffMotors(): # Turn off motors when called
    mh.getMotor(1).run(Adafruit_MotorHAT.RELEASE)
    mh.getMotor(2).run(Adafruit_MotorHAT.RELEASE)
    mh.getMotor(3).run(Adafruit_MotorHAT.RELEASE)
    mh.getMotor(4).run(Adafruit_MotorHAT.RELEASE)
    return

def fetchCurrent():
    file = open("data.csv","r")
    for line in file:
        current = line.split(",")
    currentCargo = int(current[0])
    currentPassenger = int(current[1])
    return currentCargo,currentPassenger

def storeCurrent(currentCargo,currentPassenger):
    file = open("data.csv","w")
    file.write(str(currentCargo)+","+str(currentPassenger))
    file.close()
    return

# Main script

mh = Adafruit_MotorHAT(addr = 0x60) # Create an object for the Hat
passengerStepper = mh.getStepper(513, 1) # Right motor, 513 steps
passengerStepper.setSpeed(120) # 60 RPM
cargoStepper = mh.getStepper(513,2) # Left Motor, 513 steps
cargoStepper.setSpeed(120) # 60 RPM

newCargo,newPassenger = fetchShips(); # Fetch no. of cargo and passenger ships
#newCargo = 1
#newPassenger = 1
currentCargo,currentPassenger = fetchCurrent()
differenceCargo = newCargo-currentCargo
steps = 0

for i in range(0,abs(differenceCargo)):
    steps += calculateSteps(currentCargo)
    currentCargo += 1

if differenceCargo > 0:
    cargoStepper.step(steps, Adafruit_MotorHAT.BACKWARD, Adafruit_MotorHAT.SINGLE) # Unwind
    print "Unwinding cargo: "+str(steps)
else:
    cargoStepper.step(steps, Adafruit_MotorHAT.FORWARD, Adafruit_MotorHAT.SINGLE)
    print "Winding cargo: "+str(steps)

differencePassenger = newPassenger-currentPassenger
steps = 0

for i in range(0,abs(differencePassenger)):
    steps += calculateSteps(currentPassenger)
    currentPassenger += 1

if differencePassenger > 0:
    passengerStepper.step(steps, Adafruit_MotorHAT.FORWARD, Adafruit_MotorHAT.SINGLE)
    print "Unwinding passenger: "+str(steps)
else:
    passengerStepper.step(steps, Adafruit_MotorHAT.BACKWARD, Adafruit_MotorHAT.SINGLE) #wind up
    print "Winding passenger: "+str(steps)

storeCurrent(newCargo,newPassenger)

atexit.register(turnOffMotors)